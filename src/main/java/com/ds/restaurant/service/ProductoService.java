package com.ds.restaurant.service;

import java.util.List;

import com.ds.restaurant.model.ProductoCategoria;
import com.ds.restaurant.model.ProductoDetalle;
import org.springframework.stereotype.Service;

import com.ds.restaurant.DAO.ProductoDaoImpl;
import com.ds.restaurant.model.response.ProductoResponse;


@Service
public class ProductoService extends ProductoDaoImpl {

	@Override
	public ProductoDetalle actualizarProucto(ProductoDetalle productoDetalle) throws Exception {
		return super.actualizarProucto(productoDetalle);
	}

	@Override
	public boolean eliminarProducto(int idProducto) throws Exception {
		return super.eliminarProducto(idProducto);
	}

	@Override
	public ProductoDetalle Registrar(ProductoDetalle detalle) {
		return super.Registrar(detalle);
	}

	@Override
	public List<ProductoResponse> ListarProductosxUsuario(int idusuario) throws Exception {
		// TODO Auto-generated method stub
		return super.ListarProductosxUsuario(idusuario);
	}
	
	
	@Override
	public List<ProductoResponse> ListarPorCategoria(int idusuario, String categoria) throws Exception {
		// TODO Auto-generated method stub
		return super.ListarPorCategoria(idusuario, categoria);
	}
	
	
	@Override
	public List<ProductoResponse> Buscar(int idusuario, String categoria, String busqueda) throws Exception {
		// TODO Auto-generated method stub
		return super.Buscar(idusuario, categoria, busqueda);
	}
	@Override
	public List<ProductoResponse> BuscarEnTodo(int idusuario, String busqueda) throws Exception {
		// TODO Auto-generated method stub
		return super.BuscarEnTodo(idusuario, busqueda);
	}

	@Override
	public List<ProductoCategoria> ListarCategorias() throws Exception{
		return super.ListarCategorias();
	}
}
