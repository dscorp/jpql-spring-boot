package com.ds.restaurant.service;

import com.ds.restaurant.DAO.IEmpleadoDao;
import com.ds.restaurant.model.Empleado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ds.restaurant.DAO.EmpleadoDaoImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmpleadoService extends EmpleadoDaoImpl implements IEMpleadoService,UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(EmpleadoService.class);

    @Autowired
    IEmpleadoDao iEmpleadoDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Empleado empleado = iEmpleadoDao.findByusuariologin(s);
        if (empleado == null) {
            logger.error("Error: El usuario no existe en el sistema");
            throw new UsernameNotFoundException("Error: El usuario no existe en el sistema");
        }
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        List<GrantedAuthority> authorities = Arrays.stream(new String[]{empleado.getCargo().name()})
                .map(role -> new SimpleGrantedAuthority(role))
                .peek(rolename -> logger.info("Role: " + rolename))
                .collect(Collectors.toList());
//aqui es donde comprueba si el empleado consultado mediante solo el username coincide con lo que se ingreso en el formulario
        return new User(empleado.getUsuariologin(), empleado.getContrasena(), true, true, true, true, authorities);
    }




    @Override
    public Object Login(String usuariologin, String contrasena) {
        return super.Login(usuariologin, contrasena);
    }

    @Override
    @Transactional(readOnly = true)
    public Empleado findByusuariologin(String usuariologin) {
        return iEmpleadoDao.findByusuariologin(usuariologin);
    }
}
