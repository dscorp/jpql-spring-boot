package com.ds.restaurant.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ds.restaurant.DAO.PedidoDaoImpl;
import com.ds.restaurant.model.Pedido;


@Service
public class PedidoService extends PedidoDaoImpl {


//	@Override
//	public Pedido getDetallesPedidoXidPedidoYcategoria(int idpedido) throws Exception {
//		return super.getDetallesPedidoXidPedidoYcategoria(idpedido);
//	}


	@Override
	public List<Object> getDetallesPedidoXidPedido(int idpedido) throws Exception {
		return super.getDetallesPedidoXidPedido(idpedido);
	}

	@Override
	public String pasarPedidoDeCocinaACaja(int idpedido, int numeroMesa) {
		return super.pasarPedidoDeCocinaACaja(idpedido, numeroMesa);
	}

	@Override
	public void actualizarEstadoDePedido(String estado, int idpedido) {
		super.actualizarEstadoDePedido(estado, idpedido);
	}

	@Override
	public Pedido BuscarPedidoXId(int idpedido) {
		return super.BuscarPedidoXId(idpedido);
	}

	@Override
	public List<Object> getDetallesPedidoXidPedidoYcategoria(int idpedido, String categoriaProducto) throws Exception {
		return super.getDetallesPedidoXidPedidoYcategoria(idpedido, categoriaProducto);
	}

	@Override
	public Pedido ActualizarPedidoEnCurso(Pedido pedido) throws Exception {
		// TODO Auto-generated method stub
		return super.ActualizarPedidoEnCurso(pedido);
	}

	@Override
	public Pedido RegistrarPedido(Pedido pedido) throws Exception {
		// TODO Auto-generated method stub
		return super.RegistrarPedido(pedido);
	}

	@Override
	public Pedido BuscarParaRegistrarNuevoPedido(int idpedido) throws Exception {
		// TODO Auto-generated method stub
		return super.BuscarParaRegistrarNuevoPedido(idpedido);
	}

	@Override
	public List<Object> listarPedidosEnCocinaParaAngular() throws Exception {
		return super.listarPedidosEnCocinaParaAngular();
	}

	@Override
	public Pedido ActualizarDetallePedido(Pedido pedido) throws Exception {
		// TODO Auto-generated method stub
		return super.ActualizarDetallePedido(pedido);
	}

	@Override
	public List<Pedido> ListarPorUsuario(int idMesero) {
		return super.ListarPorUsuario(idMesero);
	}
}
