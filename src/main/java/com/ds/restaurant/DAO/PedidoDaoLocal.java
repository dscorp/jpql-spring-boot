package com.ds.restaurant.DAO;

import java.util.List;

import com.ds.restaurant.model.Pedido;
import com.ds.restaurant.model.PedidoDetalle;

import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;


public interface PedidoDaoLocal {

	//lista los pedidos  con categoria comida  cib estadi enCocina
	public List<Object> listarPedidosEnCocinaParaAngular() throws Exception;
	public List<Object> getDetallesPedidoXidPedidoYcategoria(int idpedido,String categoriaProducto) throws  Exception;
	public List<Object> getDetallesPedidoXidPedido(int idpedido) throws  Exception;
	public String pasarPedidoDeCocinaACaja(int idpedido,int numeroMesa) throws SystemException, NotSupportedException;

	public void actualizarEstadoDePedido(String estado , int idpedido);

	public Pedido BuscarPedidoXId(int idpedido);



	public Pedido ActualizarPedidoEnCurso(Pedido pedido) throws Exception;
	
	public List<Pedido> ListarPorUsuario(int idMesero);

	public Pedido RegistrarPedido(Pedido pedido) throws Exception;
	
	public Pedido BuscarParaRegistrarNuevoPedido(int idpedido) throws Exception;



	
	Pedido ActualizarDetallePedido(Pedido pedido) throws Exception;
}
