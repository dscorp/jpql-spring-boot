package com.ds.restaurant.DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.*;


import com.ds.restaurant.model.Mesa;
import com.ds.restaurant.model.Pedido;
import com.ds.restaurant.model.PedidoDetalle;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PedidoDaoImpl implements PedidoDaoLocal {

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    @Override
    public List<Object> getDetallesPedidoXidPedido(int idpedido) throws Exception {
//        Query query = em.createQuery("select p from Pedido p where p.id=:p1").setParameter("p1",idpedido);
        Query query = em.createQuery("select new map(p.id as idpedido ,peddet.id as iddetallepedido, peddet.productoDetalle.producto.nombre as nombre ,peddet.candidad as cantidad, peddet.productoDetalle.precio as precio, peddet.estado as estado, peddet.productoDetalle.id as idproductodetalle, p.estado as estadopedido, p.empleado.id as idmesero)  from Pedido p  inner join p.detalles peddet  where p.id =:p1").setParameter("p1", idpedido);
//        "select new map(p.id as id,mesa.numero as mesa) from Pedido p inner join  p.mesa mesa inner join p.empleado empleado where p.estado='EnCocina' and empleado.usuariologin=:p1  ").setParameter("p1", usuario)
        return query.getResultList();
    }

    @Override
    @Transactional
    public List<Object> listarPedidosEnCocinaParaAngular() throws Exception {
        try {
            return em.createQuery("select  new map(p.id as idpedido, m.numero as numeromesa, sum(case when pdet.estado=:p3 then 1 else 0 end) as numerodetalles)  from Pedido p   inner join p.mesa m  inner join p.detalles pdet inner join  pdet.productoDetalle proddet inner join  proddet.producto prod  inner join  prod.categoria cat where p.estado=:p1 and cat.nombre=:p2 group by p.id")
                    .setParameter("p1", "EnCocina")
                    .setParameter("p2", "comida")
                    .setParameter("p3", "Preparando")
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Object> getDetallesPedidoXidPedidoYcategoria(int idpedido, String categoriaProducto) throws Exception {
//        Query query = em.createQuery("select p from Pedido p where p.id=:p1").setParameter("p1",idpedido);
        Query query = em.createQuery("select new map(p.id as idpedido ,peddet.id as iddetallepedido, peddet.productoDetalle.producto.nombre as nombre ,peddet.candidad as cantidad, peddet.productoDetalle.precio as precio, peddet.estado as estado, peddet.productoDetalle.id as idproductodetalle, p.estado as estadopedido, p.empleado.id as idmesero,p.mesa.numero as numeromesa)  from Pedido p  inner join p.detalles peddet  where p.id =:p1 and peddet.productoDetalle.producto.categoria.nombre=:p2").setParameter("p1", idpedido).setParameter("p2",categoriaProducto);
//        "select new map(p.id as id,mesa.numero as mesa) from Pedido p inner join  p.mesa mesa inner join p.empleado empleado where p.estado='EnCocina' and empleado.usuariologin=:p1  ").setParameter("p1", usuario)
        return query.getResultList();
    }

    @Override
    public String pasarPedidoDeCocinaACaja(int idpedido, int numeroMesa) {

        Long numeroDetallesDePedidoNoAtendidosPorElCocinero = (Long) em.createQuery("select  count(pd) from PedidoDetalle  pd where pd.pedido.id=:p1 and pd.estado='Preparando'").setParameter("p1", idpedido).getSingleResult();
        if (numeroDetallesDePedidoNoAtendidosPorElCocinero == 0) {
            //si todos los detalles del pedido ya han sido atendidos por el cocinero entonces ya se puede pasar a caja
            try {

                em.createQuery("update Pedido p set p.estado='EnCaja' where p.id=:idpedido").setParameter("idpedido", idpedido).executeUpdate();

                em.createQuery("update Mesa  m set m.estado=0 where m.numero=:numeroMesa ").setParameter("numeroMesa", numeroMesa).executeUpdate();

                return "ok";
            } catch (Exception e) {
                e.printStackTrace();
                return "errorTransaccion";
            }
        } else {
            return "detallesNoAtendidos";
        }


    }

    @Override
    public void actualizarEstadoDePedido(String estado, int idpedido) {
        em.createQuery("update Pedido p set p.estado=:nuevoEstado where p.id=:idpedido").setParameter("nuevoEstado", estado).setParameter("idpedido", idpedido);
    }

    @Override
    public Pedido BuscarPedidoXId(int idpedido) {
        return em.find(Pedido.class, idpedido);
    }


    @Override
    public Pedido ActualizarPedidoEnCurso(Pedido pedido) throws Exception {

        try {
//            Pedido pedTemp =em.find(Pedido.class, pedido.getId());
            for (PedidoDetalle detalle : pedido.getDetalles()) {
                detalle.setPedido(pedido);
            }
//            pedTemp.setDetalles(pedido.getDetalles());
            em.merge(pedido);
            return pedido;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Pedido> ListarPorUsuario(int idMesero) {
        try {

            Query query = em.createQuery("select new map(p.id as id,mesa.numero as mesa, p.fecha as fecha, p.empleado.nombre as nombremesero ) from Pedido p inner join  p.mesa mesa inner join p.empleado mesero where p.estado='EnCocina' and mesero.id=:p1  ").setParameter("p1", idMesero);

            return query.getResultList();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Pedido ActualizarDetallePedido(Pedido pedido) throws Exception {

        try {
            System.out.println("hola");

            PedidoDetalle ptemp = em.find(PedidoDetalle.class, pedido.getDetalles().get(0).getId());
//            Object jsonobj = em.createQuery("select new map(mesa.estado as estado, mesa.tenedores as tenedores,mesa.numero as numero) from Pedido p inner join p.mesa mesa where p.id=:p1").setParameter("p1", pedido.getId()).getResultList();
//
//            ObjectMapper mapper = new ObjectMapper();
//            String json = mapper.writeValueAsString(jsonobj);
//            json = json.substring(1, json.length() - 1);
//            Mesa mesa = mapper.readValue(json, Mesa.class);
//
            ptemp.setEstado("Listo");
            em.merge(ptemp);
            pedido.getDetalles().clear();
            pedido.getDetalles().add(ptemp);
//            pedido.setMesa(mesa);
            return pedido;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Pedido RegistrarPedido(Pedido pedido) throws Exception {

        try {
            for (PedidoDetalle detalle : pedido.getDetalles()) {
                detalle.setPedido(pedido);
            }

            Pedido p = pedido;

            em.persist(p);

            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Pedido BuscarParaRegistrarNuevoPedido(int idpedido) throws Exception {
        try {
            return (Pedido) em.createQuery("Select p from Pedido p join fetch p.mesa join fetch p.detalles pd join fetch pd.productoDetalle prodet  join fetch prodet.producto prodcuto  join fetch prodcuto.categoria categoria where p.id=:p1").setParameter("p1", idpedido).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
