package com.ds.restaurant.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;


@Repository
@Transactional
public class EmpleadoDaoImpl implements EmpleadoDaoLocal {

	
	@PersistenceContext
	private EntityManager em;
	
	
	@Override
	public Object Login(String usuariologin, String contrasena) {
		System.out.println(usuariologin);
		System.out.println(contrasena);
		Query query = em.createQuery("select new map(m.nombre as nombreempleado,m.id as idempleado,u.id as idestablecimiento, m.cargo as cargo) from Empleado m inner join m.usuario u where m.usuariologin=:p1 and m.contrasena=:p2").setParameter("p1", usuariologin).setParameter("p2", contrasena);
		return  query.getSingleResult();
	}

}
