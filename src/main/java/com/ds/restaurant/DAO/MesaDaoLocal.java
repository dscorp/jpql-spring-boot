package com.ds.restaurant.DAO;

import java.util.List;

import com.ds.restaurant.model.Mesa;

public interface MesaDaoLocal {

	public boolean comprobarEstadoMesa(Mesa mesa);
	
	public Mesa actualizarEstadoMesa(Mesa mesa,boolean estaOcupado);
	
	
	public List<Mesa> ListarMesasPorEstado (Boolean estado) throws Exception;
	
	
	
}
