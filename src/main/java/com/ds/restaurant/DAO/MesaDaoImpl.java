package com.ds.restaurant.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.ds.restaurant.model.Mesa;

@Repository
@Transactional
public class MesaDaoImpl implements MesaDaoLocal {

	@PersistenceContext
	private EntityManager em;
	
	
	@Override
	public Mesa actualizarEstadoMesa(Mesa mesa, boolean estaOcupado) {
		try {
			Mesa temp =em.find(Mesa.class, mesa.getNumero());
			temp.setEstado(estaOcupado);
			em.merge(temp);
			return temp;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean comprobarEstadoMesa(Mesa mesa) {
		Mesa temp = em.find(Mesa.class, mesa.getNumero());
		return temp.isEstado();
	}
	
	@Override
	public List<Mesa> ListarMesasPorEstado(Boolean estado) throws Exception {
	
		List<Mesa> mesas=null;
		try {
			mesas=em.createQuery("select m from Mesa m where m.estado=:p1").setParameter("p1", estado).getResultList();
			return  mesas;
		} catch (Exception e) {
			e.printStackTrace();
			return mesas;
		}
		
		
	}
	
	

}
