package com.ds.restaurant.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.ds.restaurant.model.Usuario;

@Transactional
@Repository
public class UsuarioDaoImpl implements UsuarioDaoLocal {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Usuario Login(String user, String password)  {

		Usuario usuario = null;

		Query query = entityManager.createQuery("select  u from Usuario u where u.usuario=:p1 and u.contrasena=:p2")
				.setParameter("p1", user).setParameter("p2", password);

		usuario = (Usuario) query.getSingleResult();

		return usuario;
	}

}
