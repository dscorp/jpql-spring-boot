package com.ds.restaurant.DAO;

import com.ds.restaurant.model.Empleado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IEmpleadoDao extends CrudRepository<Empleado, Integer> {

    @Query("select e from  Empleado e where e.usuariologin=?1")
public Empleado findByusuariologin(String usuariologin);
}

