package com.ds.restaurant.DAO;

import java.util.List;

import com.ds.restaurant.model.Producto;
import com.ds.restaurant.model.ProductoCategoria;
import com.ds.restaurant.model.ProductoDetalle;
import com.ds.restaurant.model.response.ProductoResponse;

public interface ProductoDaoLocal {

	public ProductoDetalle buscarXid(int idproductodetalle)throws  Exception;
	public ProductoDetalle Registrar( ProductoDetalle detalle) throws  Exception;
	public ProductoDetalle actualizarProucto(ProductoDetalle productoDetalle) throws  Exception;
	public boolean eliminarProducto(int idProducto) throws  Exception;
	public List<ProductoResponse> ListarProductosxUsuario(int estado) throws Exception;
	public List<ProductoResponse> ListarPorCategoria(int idusuario, String categoria) throws Exception;
	public List<ProductoResponse> Buscar(int idusuario, String categoria, String busqueda) throws Exception;
	public List<ProductoResponse> BuscarEnTodo(int idusuario, String busqueda )throws Exception;
	public List<ProductoCategoria> ListarCategorias() throws  Exception;
}
