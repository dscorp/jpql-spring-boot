package com.ds.restaurant.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.ds.restaurant.model.Producto;
import com.ds.restaurant.model.ProductoCategoria;
import com.ds.restaurant.model.ProductoDetalle;
import org.springframework.stereotype.Repository;

import com.ds.restaurant.model.response.ProductoResponse;

@Transactional
@Repository
public class ProductoDaoImpl implements ProductoDaoLocal {

    @PersistenceContext
    private EntityManager em;


    @Override
    public ProductoDetalle buscarXid(int idproductodetalle) throws Exception {
       return em.find(ProductoDetalle.class,idproductodetalle);
    }

    @Override
    public ProductoDetalle actualizarProucto(ProductoDetalle productoDetalle) throws Exception {
        try {

            em.merge(productoDetalle);
            return productoDetalle;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean eliminarProducto(int idProducto) throws Exception {
        try {
            ProductoDetalle p = em.find(ProductoDetalle.class, idProducto);
            p.setVigencia(false);
            em.merge(p);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ProductoDetalle Registrar(ProductoDetalle detalle) {
        em.persist(detalle);
        return detalle;
    }

    @Override
    public List<ProductoResponse> ListarProductosxUsuario(int idusuario) throws Exception {

        Query query = em.createQuery(
                "select new map  (pd.id as id, p.nombre as nombre, p.descripcion as descripcion , p.imagen as imagen, p.valoracion as valoracion, pd.precio as percio) from Producto p  inner join p.detalles pd inner join p.usuario u   where pd.vigencia=1 and u.id=:p1")
                .setParameter("p1", idusuario);

        return (List<ProductoResponse>) query.getResultList();
    }

    @Override
    public List<ProductoResponse> ListarPorCategoria(int idusuario, String categoria) throws Exception {
        Query query = em.createQuery(
                "select new map  (pd.id as id, p.nombre as nombre, p.descripcion as descripcion , p.imagen as imagen, p.valoracion as valoracion, pd.precio as precio, cat.nombre as categoria) from Producto p  inner join p.detalles pd inner join p.usuario u inner join p.categoria c  inner join p.categoria cat where pd.vigencia=1 and u.id=:p1 and c.nombre=:p2 ")
                .setParameter("p1", idusuario).setParameter("p2", categoria);

        return (List<ProductoResponse>) query.getResultList();
    }

    @Override
    public List<ProductoResponse> Buscar(int idusuario, String categoria, String busqueda) throws Exception {
        Query query = em.createQuery(
                "select new map  (pd.id as id, p.nombre as nombre, p.descripcion as descripcion , p.imagen as imagen, p.valoracion as valoracion, pd.precio as precio, c.nombre as categoria) from Producto p  inner join p.detalles pd inner join p.usuario u inner join p.categoria c  where pd.vigencia=1 and u.id=:p1 and c.nombre=:p2 and p.nombre like concat('%',:p3,'%') ")
                .setParameter("p1", idusuario).setParameter("p2", categoria).setParameter("p3", busqueda);

        return (List<ProductoResponse>) query.getResultList();
    }

    @Override
    public List<ProductoResponse> BuscarEnTodo(int idusuario, String busqueda) throws Exception {
        Query query = em.createQuery(
                "select new map  (pd.id as id, p.nombre as nombre, p.descripcion as descripcion , p.imagen as imagen, p.valoracion as valoracion, pd.precio as precio, c.nombre as categoria) from Producto p  inner join p.detalles pd inner join p.usuario u inner join p.categoria c  where pd.vigencia=1 and u.id=:p1  and p.nombre like concat('%',:p3,'%') ")
                .setParameter("p1", idusuario).setParameter("p3", busqueda);

        return (List<ProductoResponse>) query.getResultList();
    }

    @Override
    public List<ProductoCategoria> ListarCategorias() throws Exception {
        Query query = em.createQuery("select c from ProductoCategoria c ");
        return query.getResultList();
    }

}
