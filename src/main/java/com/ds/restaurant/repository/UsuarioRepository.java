package com.ds.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ds.restaurant.model.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
