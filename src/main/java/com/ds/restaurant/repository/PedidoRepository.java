package com.ds.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ds.restaurant.model.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Integer>{

}
