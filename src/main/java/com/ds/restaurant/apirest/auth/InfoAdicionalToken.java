package com.ds.restaurant.apirest.auth;

import com.ds.restaurant.model.Empleado;
import com.ds.restaurant.service.IEMpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

    @Autowired
    private IEMpleadoService ieMpleadoService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Empleado empleado = ieMpleadoService.findByusuariologin(authentication.getName());
        Map<String,Object> info = new HashMap<>();
        info.put("id_empleado",empleado.getId());
        info.put("username_empleado",empleado.getUsuariologin());
        info.put("dni_empleado",empleado.getDni());
        info.put("nombre_empleado",empleado.getNombre());
        info.put("cargo_empleado",empleado.getCargo());
        info.put("id_empresa",empleado.getUsuario().getId());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
        return accessToken;
    }
}
