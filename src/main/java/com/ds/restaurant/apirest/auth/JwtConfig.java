package com.ds.restaurant.apirest.auth;

import org.bouncycastle.jcajce.provider.asymmetric.RSA;

public class JwtConfig {
    public static final String LLAVE_SECRETA = "dscorp";
    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAwJwavKbrLwBu8ZhhKZJGxUOCaWfPm0oa++kfXsS5dm2DE2fg\n" +
            "qccA15Er2/tfkxcfYw3JPkHoiI56bPPP0WxdKBYL3EY70xy6v80c5yHSWBelyx+Q\n" +
            "3IuG53Gs54ydAwK9mRw48IFr9RxHI8n9BlRnXfrukL37bGIQQWlpwOTy5VkWJRRt\n" +
            "lidHQe70Unsm0XR3r8jjsHtwPrM17FC45lU0mEMlKtM3+U6cGxA8m7G4MGy/fQPE\n" +
            "XzWTp/eW0rEdkJvKbcFPIUDuT98hzeXio5yi9s7WiSDrxZrv06Eu7fhfIswa0N9X\n" +
            "ltmpkVRfr0nGDiiqoH7AS3YJaWC0L3oKUTkw0wIDAQABAoIBADcJpF45+ERDBHDG\n" +
            "0STW5qAhrVc/UcSXBWTsdYlX/4MGwgi+iyUYd3Yhxn2is+nNFqvwWYwPu0miJPuT\n" +
            "I9Q8+dWljxgPfbLSp8585wBo2rlPcJG9YGCVAiSKiBTWSlt84f6pYN/gq5+rx+xD\n" +
            "IKk5XxwswxTbJMgQsYJvg8eZ5IqCTT5uitcE1wfX5JfETztm5gqz3EQ7uD+jHa1G\n" +
            "2R5leCJdtQ8qfgr/BruWeBFIIWMfZqeNlGQbKz2v/yPzFC1V+mbJEKMmD3RpJ7Q9\n" +
            "n4s10eiJVjQMJgl4RUK1ZO+SqspgXr4YEElaq3Ap3moshHo6pIyu7rxQ08u9oHHq\n" +
            "hMS+vTECgYEA/y8D72P5+tRACvCqe/bGD54+/pQiQ5oL4r8TOaMMYeHPIR6avqIz\n" +
            "HGkkzFxu+QfsyohKOH7yR2NwGny5ge5HdwuclvlCNJ6r221hRCUY98fSEi4IZrjw\n" +
            "BcVHi0B4pTZu3FOnpwRxHfW/Pq85IdBl65Q4CwgW6+xqQdklf6mbC8kCgYEAwTnX\n" +
            "/Z77Tmo5yz6BrnsSH7RWVO5OwE2AcV5dB1TpLgf/XO62hex6y/q80IBv49FllxyA\n" +
            "Z/gODFIns55R8DESCwDf0PbsabA1LGl+rPIeDqRwAnCwkjSRA1wVRDFIvXoX+d9f\n" +
            "G+5hpRBSLbL+cBDhUM7Lj+K+Wm9zaomwdrTbbbsCgYBPdJ4vDdwqQ6hlP6Z1ztLA\n" +
            "zi8jKZC5p9+HjKM50RDr8VmDIHpiXTK45+wzTZyoIdWTAnjJuo1JuSaQuMEyObU6\n" +
            "1fy97YK1y6oLOrWMeFib3jYEvxySj4b85P5v13U5TAecxtdu460s/PW9WOXbJ350\n" +
            "qcQBe1VXxFcZhbltVKwYMQKBgQCsMUMVJi9oAuv7/nkMp2pQTKI+z5vobWiQHvqi\n" +
            "RP4D4makR6rqkAIicxD+cebES9/PmTuo63qBalktDWKRvPkKSz4pd1LZIIlSSbT0\n" +
            "pFM60zlqQ0hnEmdEajQdcBoVmI9zQqb8OiRUN8Y0KXXrIm/YVmE7mWHvL7DX+VoD\n" +
            "f0qobwKBgQDglKSvc/7V4kU1doK9vHy+EPOGemAbIKiK2g24Mjy0mGGxeVhCPjU9\n" +
            "soe3lybBNk1DMXX5USgYaOxQ8dTI4cUv8RbiO4IVSgJn6b+nLqZHqwtl7+aqKJ5a\n" +
            "1Q7QP8rLB/HTIzw2QbqtlW5x0XrN7L6fSjJ4+1XHWF8qgVnGMXyTfA==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwJwavKbrLwBu8ZhhKZJG\n" +
            "xUOCaWfPm0oa++kfXsS5dm2DE2fgqccA15Er2/tfkxcfYw3JPkHoiI56bPPP0Wxd\n" +
            "KBYL3EY70xy6v80c5yHSWBelyx+Q3IuG53Gs54ydAwK9mRw48IFr9RxHI8n9BlRn\n" +
            "XfrukL37bGIQQWlpwOTy5VkWJRRtlidHQe70Unsm0XR3r8jjsHtwPrM17FC45lU0\n" +
            "mEMlKtM3+U6cGxA8m7G4MGy/fQPEXzWTp/eW0rEdkJvKbcFPIUDuT98hzeXio5yi\n" +
            "9s7WiSDrxZrv06Eu7fhfIswa0N9XltmpkVRfr0nGDiiqoH7AS3YJaWC0L3oKUTkw\n" +
            "0wIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
