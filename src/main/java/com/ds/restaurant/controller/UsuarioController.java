package com.ds.restaurant.controller;


import java.util.HashMap;
import java.util.Map;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ds.restaurant.model.Usuario;
import com.ds.restaurant.service.UsuarioService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService service;

	
//	@PostMapping(value = "/registrar")
//	public ResponseEntity<?> registrar(@RequestBody Usuario usuario) {
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		Usuario usuarioregistrado = null;
//		try {
//			usuarioregistrado = service.Registrar(usuario);
//			map.put("error", false);
//			map.put("mensaje", "Regitro Exitoso");
//			map.put("productos", usuarioregistrado);
//			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//
//		} catch (DataAccessException e) {
//
//			map.put("error", true);
//			map.put("mensaje", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
//			map.put("productos", null);
//			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//
//	}
	
	
	@PostMapping(value = "/login")
	public ResponseEntity<?> login (@RequestParam String user , @RequestParam String password){
		
		Usuario userResponse=null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			userResponse=service.Login(user, password);
			
			if(userResponse==null)
			{
		
			}
			map.put("error", false);
			map.put("mensaje", "Bienvenido ".concat(userResponse.getNombreEstablecimiento()));
			map.put("usuario", userResponse);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch (Exception e) {
			
			
			
		e.printStackTrace();
			if(e instanceof EmptyResultDataAccessException)
			{
				map.put("error", false);
				map.put("mensaje", "Credenciales incorrectas");
				map.put("usuario", null);
				return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
			}
			
			
			map.put("error", true);
			map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
			map.put("usuario", null);
			// TODO: handle exception 
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

}
