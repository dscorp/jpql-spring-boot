package com.ds.restaurant.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ds.restaurant.service.MesaService;
import com.ds.restaurant.service.PedidoService;
import com.ds.restaurant.util.VariablesEstaticas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ds.restaurant.model.Pedido;
import com.ds.restaurant.model.PedidoDetalle;

import javax.swing.*;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;

//@CrossOrigin
@RestController
@RequestMapping("/pedido")
public class PedidoController implements Serializable {


    @Autowired
    private PedidoService service;

    @Autowired
    private MesaService mesaService;

    //esto sirve para poder enviar mensajes a travez del broker de stomp pero desde un metodo rest
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;


    //se usa para ver los detalles en angular  con categoriaProducto= comida
    //se usa para ver los detalles en android con categoriaProducto = bebidas

    @GetMapping("/getDetallesPedidoXidPedidoCat")
    public ResponseEntity<Map<String, Object>> getDetallesPedidoXidPedido(@RequestParam int idpedido,@RequestParam String categoriaProducto) {

        Map<String, Object> map = new HashMap<>();
        try {
            map.put("error", false);
            map.put("mensaje", "Solicitud Exitosa");
            map.put("detallespedido", service.getDetallesPedidoXidPedidoYcategoria(idpedido,categoriaProducto));

        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la BD");
                map.put("detallespedido", null);
            } else {
                map.put("error", true);
                map.put("mensaje", e.getMessage());
                map.put("detallespedido", null);
            }
        }
        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }


    @GetMapping("/getDetallesPedidoXidPedido")
    public ResponseEntity<Map<String, Object>> getDetallesPedidoXidPedido(@RequestParam int idpedido) {
        Map<String, Object> map = new HashMap<>();
        try {
            map.put("error", false);
            map.put("mensaje", "Solicitud Exitosa");
            map.put("detallespedido", service.getDetallesPedidoXidPedido(idpedido));

        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la BD");
                map.put("detallespedido", null);
            } else {
                map.put("error", true);
                map.put("mensaje", e.getMessage());
                map.put("detallespedido", null);
            }
        }
        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }




    @PostMapping("/actualizarDetPedido")
    public Map<String, Object> actualizaconDetallePedido(@RequestBody Pedido pedido) throws Exception {
        Pedido pedtemp;
        Map<String, Object> map = new HashMap<>();
        try {

            pedtemp = service.ActualizarDetallePedido(pedido);
            map.put("error", false);
            map.put("mensaje", "Actualizacion correcta");
            map.put("pedido", pedtemp);
            simpMessagingTemplate.convertAndSend("/restaurant/actualizacionDet",map);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error al actualizar");
            map.put("pedido", null);
            return map;
        }

    }

    @PostMapping("/nuevoPedido")
    public Map<String, Object> NuevoPedido(@RequestBody Pedido pedido) throws Exception {
        Map<String, Object> map = new HashMap<>();
        Pedido pp = null;
        try {
            //si la mesa no esta ocupada
            if(mesaService.comprobarEstadoMesa(pedido.getMesa())==false)
            {
                Pedido p = service.RegistrarPedido(pedido);
                mesaService.actualizarEstadoMesa(p.getMesa(),true);

                pp = service.BuscarParaRegistrarNuevoPedido(p.getId());
                List<PedidoDetalle> newdetalles = new ArrayList<>();
                for (PedidoDetalle detalle : pp.getDetalles()) {
                    if (detalle.getProductoDetalle().getProducto().getCategoria().getNombre()
                            .equals(VariablesEstaticas.CategoriaComida)) {
                        newdetalles.add(detalle);
                    }
                }
                pp.setDetalles(newdetalles);

                map.put("error", false);
                map.put("mensaje", "Respuesta Exitosa");
                map.put("pedido", pp);
                this.simpMessagingTemplate.convertAndSend("/restaurant/nuevopedido",map);
                return map;
            }
            else {
                map.put("error", false);
                map.put("mensaje", "La mesa ya se encuentra ocupada, Consulte con el cocinero");
                map.put("pedido", null);
                return map;
            }

        } catch (Exception e) {
            e.printStackTrace();

            if (e instanceof javax.persistence.NoResultException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la BD");
                map.put("pedido", null);
                return map;
            } else {
                map.put("error", true);
                map.put("mensaje", "Ocurrio un error : " + e.getMessage());
                map.put("pedido", null);
                return map;
            }
        }

    }


    @PostMapping("/actualizarPedidoEnCurso")
    public Map<String, Object> ActualizarPedidoEnCurso(@RequestBody Pedido pedido) throws Exception {
        Pedido pedtemp;
        Map<String, Object> map = new HashMap<>();
        try {
//			pedtemp = service.ActualizarDetallePedido(pedido);
            pedtemp=service.ActualizarPedidoEnCurso(pedido);
            map.put("error", false);
            map.put("mensaje", "El pedido fue actualizado correctamente");
            map.put("pedido", pedtemp);
            this.simpMessagingTemplate.convertAndSend("/restaurant/reespuestaActualizarcionPedidoEnCurso",map);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error al actualizar el pedido :"+ e.getMessage());
            map.put("pedido", null);
            return map;
        }

    }


    @PostMapping("/pasarPedidoDeCocinaACaja")
    public ResponseEntity<Map<String, Object>> pasarPedidoDeCocinaACaja(@RequestParam int idpedido, @RequestParam int numeroMesa) throws NotSupportedException, SystemException {
        Map<String, Object> map = new HashMap<>();
        String resultado = service.pasarPedidoDeCocinaACaja(idpedido, numeroMesa);
        switch (resultado) {
            case "ok":
                map.put("error", false);
				map.put("mensaje", "El pedido fue enviado a CAJA");

//                try {
                    this.simpMessagingTemplate.convertAndSend("/restaurant/respuestApasarPedidoDeCocinaACaja","mira mi respueston papu");
//                } catch (MessagingException e) {
//                    e.printStackTrace();
//                    this.simpMessagingTemplate.convertAndSend("/restaurant/respuestApasarPedidoDeCocinaACaja","mira mi respueston papu");
//                }
                break;
            case "errorTransaccion":
                map.put("error", true);
                map.put("mensaje", "Ocurrio un error Interno del servidor,  Por favor intente nuevamente en unos minutos");
                break;

            case "detallesNoAtendidos":
                map.put("error", true);
                map.put("mensaje", "Error: El pedido tiene items  pendientes en cocina");
                break;
        }


        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }








    @GetMapping("/listarPedidosEnCursoxUsuario")
    public ResponseEntity<Map<String, Object>> listarPedidosEnCursoxUsuario(@RequestParam int idMesero) {

        Map<String, Object> map = new HashMap<>();
        try {
            map.put("error", false);
            map.put("mensaje", "Solicitud Exitosa");
            map.put("pedidosencursousuario",service.ListarPorUsuario(idMesero));
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la BD");
                map.put("pedidosencursousuario", null);
            } else {
                map.put("error", true);
                map.put("mensaje", e.getMessage());
                map.put("pedidosencursousuario", null);
            }
        }

        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }


    @PostMapping("/registrar")
    public ResponseEntity<?> RegistrarPedido(@RequestBody Pedido pedido) throws Exception {
        for (PedidoDetalle detalle : pedido.getDetalles()) {
            detalle.setPedido(pedido);
        }
        Pedido p = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            service.RegistrarPedido(pedido);
            map.put("error", false);
            map.put("mensaje", "El registro fue exitoso");
            map.put("Pedido", p);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "ocurrio un error: ".concat(e.getCause().toString()));
            map.put("Pedido", null);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/listarPedidosEnCocinaParaAngular")
    public ResponseEntity<Map<String, Object>> listarPedidosEnCocinaParaAngular() {

        Map<String, Object> map = new HashMap<>();
        try {
            map.put("error", false);
            map.put("mensaje", "Solicitud Exitosa");
            map.put("pedidos", service.listarPedidosEnCocinaParaAngular());
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la BD");
                map.put("pedidos", null);
            } else {
                map.put("error", true);
                map.put("mensaje", e.getMessage());
                map.put("pedidos", null);
            }
        }

        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }


}
