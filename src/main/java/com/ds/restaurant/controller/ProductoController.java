package com.ds.restaurant.controller;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ds.restaurant.model.ProductoCategoria;
import com.ds.restaurant.model.ProductoDetalle;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ds.restaurant.model.response.ProductoResponse;
import com.ds.restaurant.service.ProductoService;
import org.springframework.web.multipart.MultipartFile;


@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoService service;

//	
//	@Autowired
//	private ProductoRepository productoService;


    @GetMapping("/buscarxid/{id}")
    public ResponseEntity<?> buscarXid(@PathVariable int id) {
       Map<String,Object> map = new HashMap<>();
        try {
        map.put("error",false);
        map.put("mensaje","Solicitud Exitosa");
        map.put("productodetalle", service.buscarXid(id));
        return new ResponseEntity<>(map,HttpStatus.OK);
        } catch (Exception e) {
            map.put("error",true);
            map.put("mensaje",e.getMessage());
            map.put("productodetalle",null);
            return new ResponseEntity<>(map,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/actualizar")
    public ResponseEntity<?> actualizarProducto(@RequestBody ProductoDetalle productoDetalle) {
        Map<String, Object> map = new HashMap<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            System.out.println(mapper.writeValueAsString(productoDetalle));
            ProductoDetalle pd = service.actualizarProucto(productoDetalle);
            map.put("error", false);
            map.put("mensaje", "El producto fue actualizado correctamente");
            map.put("productodetalle", pd);

            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            map.put("error", true);
            map.put("mensaje", e.getMessage());
            map.put("productodetalle", null);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/eliminar/{idproductodetalle}")
    public ResponseEntity<?> eliminarProducto(@PathVariable int idproductodetalle) {
        Map<String, Object> map = new HashMap<>();
        try {
            service.eliminarProducto(idproductodetalle);
            map.put("error", false);
            map.put("mensaje", "Producto eliminado correctamente");
            return new ResponseEntity<>(map, HttpStatus.OK);


        } catch (Exception e) {
            map.put("error", true);
            map.put("mensaje", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/categorias")
    public ResponseEntity<?> listarCategorias() {

        List<ProductoCategoria> categorias = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            categorias = service.ListarCategorias();
            map.put("error", false);
            map.put("Mensaje", "Solicitud exitosa");
            map.put("categorias", categorias);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la tabla");
                map.put("categorias", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            } else {
                map.put("error", true);
                map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
                map.put("categorias", null);

                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }


    //    @Secured({"ROLE_MESERO"})
    @PostMapping("/registrar")
    public ResponseEntity<?> registrar(@RequestParam String detalle, @RequestParam MultipartFile imagen) {
        System.out.println(detalle);
        Map<String, Object> map = new HashMap<String, Object>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {

            ProductoDetalle detalle2 = objectMapper.readValue(detalle, ProductoDetalle.class);
            System.out.println(objectMapper.writeValueAsString(detalle2));
            Date date = new Date();
            String nombreArchivo = date.getTime() + imagen.getOriginalFilename();
            Path rutaArchivo = Paths.get("images").resolve(nombreArchivo).toAbsolutePath();
            Files.copy(imagen.getInputStream(), rutaArchivo);
            detalle2.getProducto().setImagen(nombreArchivo);
            service.Registrar(detalle2);
            map.put("error", false);
            map.put("Mensaje", "Producto Registrado Correctamente");
            map.put("productodetalle", detalle2);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
            map.put("productodetalle", null);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/images/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto) {
        Path rutaArchivo = Paths.get("images").resolve(nombreFoto).toAbsolutePath();
        Resource recurso = null;

        try {
            recurso = new UrlResource(rutaArchivo.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (!recurso.exists() && !recurso.isReadable()) {
            throw new RuntimeException("Error no se pudo cargar la imagen: " + nombreFoto);
        }

        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");
        return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
    }


    @GetMapping("/listarxusuario")
    ResponseEntity<?> listarxestado(@RequestParam int idusuario) {
        List<ProductoResponse> productos = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            productos = service.ListarProductosxUsuario(idusuario);
            map.put("error", false);
            map.put("Mensaje", "Solicitud exitosa");
            map.put("productos", productos);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la tabla");
                map.put("usuario", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            } else {
                map.put("error", true);
                map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
                map.put("usuario", null);

                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
            }


        }


    }

    @GetMapping("/buscartodo")
    ResponseEntity<?> buscarTodo(@RequestParam int idusuario, @RequestParam String busqueda) {

        List<ProductoResponse> productos = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {

            productos = service.BuscarEnTodo(idusuario, busqueda);
            map.put("error", false);
            map.put("Mensaje", "Solicitud exitosa");
            map.put("productos", productos);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la tabla");
                map.put("usuario", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            }
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
            map.put("usuario", null);

            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }

    @GetMapping("/listarxcategoria")
    ResponseEntity<?> listarxCategoria(@RequestParam int idusuario, @RequestParam String categoria) {
        List<ProductoResponse> productos = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            System.out.println("ProductoController.listarxCategoria()");
            productos = service.ListarPorCategoria(idusuario, categoria);
            map.put("error", false);
            map.put("Mensaje", "Solicitud exitosa");
            map.put("productos", productos);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la tabla");
                map.put("usuario", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            }
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
            map.put("usuario", null);

            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }


    @GetMapping("/buscar")
    ResponseEntity<?> Buscar(@RequestParam int idusuario, @RequestParam String categoria, String busqueda) {
        System.out.println(categoria + " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        List<ProductoResponse> productos = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            System.out.println("ProductoController.listarxCategoria()");
            productos = service.Buscar(idusuario, categoria, busqueda);
            map.put("error", false);
            map.put("Mensaje", "Solicitud exitosa");
            map.put("productos", productos);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", false);
                map.put("mensaje", "No se encontraron datos en la tabla");
                map.put("usuario", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            }
            map.put("error", true);
            map.put("mensaje", "Ocurrio un error: ".concat(e.getMessage()));
            map.put("usuario", null);

            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }


//	@RequestMapping(value = "/listarplatos", method = RequestMethod.GET)
//	@GetMapping("/listarplatos")
//	ResponseEntity<?> listarPlatos() {
//		List<Producto> productos = null;
//		Map<String, Object> map = new HashMap<>();
//
//		try {
//
//			productos = productoService.findAll();
//		
//			if(productos.size()==0)
//			{
//
//				map.put("error", false);
//				map.put("mensaje", "No existen registros en la base de datos");
//				map.put("productos", productos);
//				
//				return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//			}
//			
//			map.put("error", false);
//			map.put("mensaje", "consulta exitosa");
//			map.put("productos", productos);
//			
//			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
//		} catch (Exception e) {
//			map.put("error", true);
//			map.put("mensaje",e.getMessage()+" : "+e.getMessage());
//			map.put("productos", null);
//			
//			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//
//		
//	}

}
