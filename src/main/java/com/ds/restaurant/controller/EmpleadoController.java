package com.ds.restaurant.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ds.restaurant.service.EmpleadoService;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/empleado")
public class EmpleadoController {

    @Autowired
    private EmpleadoService service;


    @PostMapping("login")
    public ResponseEntity<?> login(@RequestParam String usuariologin, @RequestParam String contrasena) {
        Object mesero = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            mesero = service.Login(usuariologin, contrasena);
            Map<String, String> mtemp = (Map<String, String>) mesero;
            map.put("error", false);
            map.put("mensaje", "Bienvenido: ".concat(mtemp.get("nombreempleado").toUpperCase()));
            map.put("empleado", mesero);
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            if (e instanceof EmptyResultDataAccessException) {
                map.put("error", true);
                map.put("mensaje", "Credenciales incorrectas");
                map.put("empleado", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
            } else {
                map.put("error", true);
                map.put("mensaje", "Ocurrio un error: ".concat(e.getCause().toString()));
                map.put("empleado", null);
                return new ResponseEntity<Map<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }


}
