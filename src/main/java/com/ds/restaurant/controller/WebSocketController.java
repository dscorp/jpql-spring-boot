package com.ds.restaurant.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ds.restaurant.DAO.PedidoDaoImpl;
import com.ds.restaurant.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.ds.restaurant.model.Pedido;
import com.ds.restaurant.model.PedidoDetalle;
import com.ds.restaurant.service.MesaService;

import com.ds.restaurant.util.VariablesEstaticas;


@Controller
public class WebSocketController {

	@Autowired
	PedidoService service;
	
	@Autowired MesaService mesaService;


//	@MessageMapping("/actualizarPedidoEnCurso")
//	@SendTo("/restaurant/reespuestaActualizarcionPedidoEnCurso")
//	public Map<String, Object> ActualizarPedidoEnCurso(Pedido pedido) throws Exception {
//
//		System.out.println(pedido);
//
//
//		Pedido pedtemp;
//		Map<String, Object> map = new HashMap<>();
//		try {
////			pedtemp = service.ActualizarDetallePedido(pedido);
//			pedtemp=service.ActualizarPedidoEnCurso(pedido);
//			map.put("error", false);
//			map.put("mensaje", "Actualizacion correcta");
//			map.put("pedido", pedtemp);
//			return map;
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("error", true);
//			map.put("mensaje", "Ocurrio un error al actualizar");
//			map.put("pedido", null);
//			return map;
//		}
//
//	}
//
	
	
//	@MessageMapping("/actualizarDetPedido")
//	@SendTo("/restaurant/actualizacionDet")
//	public Map<String, Object> actualizaconDetallePedido(Pedido pedido) throws Exception {
//		Pedido pedtemp;
//		Map<String, Object> map = new HashMap<>();
//		try {
//			pedtemp = service.ActualizarDetallePedido(pedido);
//			map.put("error", false);
//			map.put("mensaje", "Actualizacion correcta");
//			map.put("pedido", pedtemp);
//			return map;
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("error", true);
//			map.put("mensaje", "Ocurrio un error al actualizar");
//			map.put("pedido", null);
//			return map;
//		}
//
//	}

	
//	@MessageMapping("/npedido")
//	@SendTo("/restaurant/nuevopedido")
//	public Map<String, Object> NuevoPedido(Pedido pedido) throws Exception {
//		Map<String, Object> map = new HashMap<>();
//		Pedido pp = null;
//
//		try {
//
//			//si la mesa esta ocupada
//			if(mesaService.comprobarEstadoMesa(pedido.getMesa())==false)
//			{
//				Pedido p = service.RegistrarPedido(pedido);
//
//				mesaService.actualizarEstadoMesa(p.getMesa(),true);
//				pp = service.BuscarParaRegistrarNuevoPedido(p.getId());
//
//				List<PedidoDetalle> newdetalles = new ArrayList<>();
//				for (PedidoDetalle detalle : pp.getDetalles()) {
//					if (detalle.getProductoDetalle().getProducto().getCategoria().getNombre()
//							.equals(VariablesEstaticas.CategoriaComida)) {
//						newdetalles.add(detalle);
//					}
//				}
//
//				pp.setDetalles(newdetalles);
//
//				map.put("error", false);
//				map.put("mensaje", "Respuesta Exitosa");
//				map.put("pedido", pp);
//
//				return map;
//			}
//			else {
//				map.put("error", false);
//				map.put("mensaje", "La mesa ya se encuentra ocupada, Consulte con el cocinero");
//				map.put("pedido", null);
//				return map;
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//
//			if (e instanceof javax.persistence.NoResultException) {
//				map.put("error", false);
//				map.put("mensaje", "No se encontraron datos en la BD");
//				map.put("pedido", null);
//				return map;
//			} else {
//				map.put("error", true);
//				map.put("mensaje", "Ocurrio un error : " + e.getMessage());
//				map.put("pedido", null);
//				return map;
//			}
//		}
//
//	}

}
