package com.ds.restaurant.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ds.restaurant.model.Mesa;
import com.ds.restaurant.service.MesaService;
@CrossOrigin(origins="*")
@RestController
@RequestMapping("/mesa")
public class MesaController {

	@Autowired
	private MesaService service;	
	
	@GetMapping("/listarxestado")
	public ResponseEntity<?> listarxestado(@RequestParam boolean estado) {
		List<Mesa> mesas = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			mesas=service.ListarMesasPorEstado(estado);
			map.put("error", false);
			map.put("mensaje", "Solicitud Exitosa");
			map.put("Mesas", mesas);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch (Exception e) {
			if(e instanceof EmptyResultDataAccessException)
			{
				map.put("error", false);
				map.put("mensaje", "No existen registros en la BD");
				map.put("Mesas", null);
				return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
			}
			
			map.put("error", true);
			map.put("mensaje", "No existen registros en la BD");
			map.put("Mesas", null);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	
		
	}

}
