package com.ds.restaurant.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Empleado implements Serializable {

	
	@GeneratedValue
	@Id
	private int id;
	private String usuariologin;
	private String contrasena; 
	private String nombre;
	private String dni;

	public enum  enumCargos{ROLE_COCINERO,ROLE_MESERO};
	@Enumerated(EnumType.STRING)
	private enumCargos cargo;

	@OneToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;
	
 
	@JsonIgnore
	@OneToMany(mappedBy = "empleado")
	private List<Pedido> pedidos;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUsuariologin() {
		return usuariologin;
	}


	public void setUsuariologin(String usuariologin) {
		this.usuariologin = usuariologin;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public List<Pedido> getPedidos() {
		return pedidos;
	}


	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public enumCargos getCargo() {
		return cargo;
	}

	public void setCargo(enumCargos cargo) {
		this.cargo = cargo;
	}
}
