package com.ds.restaurant.model.response;

import com.ds.restaurant.model.Pedido;

public class PedidoResponse {

	private boolean error;
	private String Mensaje;
	private Pedido pedido;
	
	
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getMensaje() {
		return Mensaje;
	}
	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	
	

	
}
