package com.ds.restaurant.model;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PedidoDetalle {

	@Id
	@GeneratedValue
	private int id;
	
	private int candidad; 
	@Column(columnDefinition = "ENUM('Preparando','Listo')")
	private String estado;
	


	@OneToOne
	@JoinColumn(name = "idProductoDetalle")
	private ProductoDetalle productoDetalle;

	

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idpedido")
	private Pedido pedido;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getCandidad() {
		return candidad;
	}


	public void setCandidad(int candidad) {
		this.candidad = candidad;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}



	public ProductoDetalle getProductoDetalle() {
		return productoDetalle;
	}


	public void setProductoDetalle(ProductoDetalle productoDetalle) {
		this.productoDetalle = productoDetalle;
	}


	public Pedido getPedido() {
		return pedido;
	}


	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	


}
