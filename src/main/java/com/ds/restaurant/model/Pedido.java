package com.ds.restaurant.model;

import java.io.Serializable;
import java.util.Date;

import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "Pedido")
public class Pedido  implements Serializable {

	@Id
	@GeneratedValue
	private int id;
 
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha = new Date();
	
	@Column(columnDefinition = "ENUM('EnCocina','EnCaja','Listo')")
	private String estado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="numeroMesa")
	private Mesa mesa;



	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEmpleado")
	public Empleado empleado;
	
	
	@OneToMany(mappedBy = "pedido",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<PedidoDetalle> detalles;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}


	public List<PedidoDetalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<PedidoDetalle> detalles) {
		this.detalles = detalles;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", fecha=" + fecha + ", estado=" + estado + ", mesa=" + mesa + ", empleado=" + empleado
				+ ", detalles=" + detalles + "]";
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
}
