package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;

import java.util.List;

public class Pedido  implements Serializable {

	private static final long serialVersionUID = 8177742338042183952L;


	private int id;


//	@Column(columnDefinition = "ENUM('EnCocina','Listo')")
	private String estado;
	private Mesa mesa;

	public Pedido(Mesa mesa, Mesero mesero, List<PedidoDetalle> detalles) {
		this.estado="EnCocina";
		this.mesa = mesa;
		this.mesero = mesero;
		this.detalles = detalles;
	}

	public Pedido() {
	}

	private Mesero mesero;

	private List<PedidoDetalle> detalles;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Mesero getMesero() {
		return mesero;
	}

	public void setMesero(Mesero mesero) {
		this.mesero = mesero;
	}

	public List<PedidoDetalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<PedidoDetalle> detalles) {
		this.detalles = detalles;
	}



}
