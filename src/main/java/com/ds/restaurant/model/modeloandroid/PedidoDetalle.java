package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PedidoDetalle implements Serializable {

	private static final long serialVersionUID = -8864766423624850968L;

	private int id;

	private int candidad;
//	@Column(columnDefinition = "ENUM('Preparando','Listo')")
	private String estado;

	private Producto productoDetalle;

	@JsonIgnore
	private Pedido pedido;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCandidad() {
		return candidad;
	}

	public void setCandidad(int candidad) {
		this.candidad = candidad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Producto getProductoDetalle() {
		return productoDetalle;
	}

	public void setProductoDetalle(Producto productoDetalle) {
		this.productoDetalle = productoDetalle;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}



}
