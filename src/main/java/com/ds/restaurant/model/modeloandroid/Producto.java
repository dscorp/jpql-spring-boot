package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;

public class Producto implements Serializable{


	private static final long serialVersionUID = 8657825865105251495L;
	
	
	private String descripcion;
	private String imagen;
 	private Integer valoracion;
 	private Integer id;
 	private String nombre;
 	private Double percio;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Integer getValoracion() {
		return valoracion;
	}

	public void setValoracion(Integer valoracion) {
		this.valoracion = valoracion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPercio() {
		return percio;
	}

	public void setPercio(Double percio) {
		this.percio = percio;
	}

}