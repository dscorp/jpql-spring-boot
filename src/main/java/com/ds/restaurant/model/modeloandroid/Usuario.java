package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;
import java.util.List;

public class Usuario  implements Serializable {

	private static final long serialVersionUID = -1688654466994960224L;

	private int id;

	private String nombreEstablecimiento;

	private String usuario;
	private String contrasena;

	private List<Producto> productos;

	private List<Mesero> meseros;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombreEstablecimiento() {
		return nombreEstablecimiento;
	}


	public void setNombreEstablecimiento(String nombreEstablecimiento) {
		this.nombreEstablecimiento = nombreEstablecimiento;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	public List<Producto> getProductos() {
		return productos;
	}


	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}


	public List<Mesero> getMeseros() {
		return meseros;
	}


	public void setMeseros(List<Mesero> meseros) {
		this.meseros = meseros;
	}
	

	
	

}
