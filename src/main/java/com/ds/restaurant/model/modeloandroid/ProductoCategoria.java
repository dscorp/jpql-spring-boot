package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;
import java.util.List;


public class ProductoCategoria implements Serializable {

	private static final long serialVersionUID = 8956796327866952217L;
	private int id;
	private String nombre;
	private String descripcion;
	private String imagen;

	private List<Producto> productos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
	
	

	
	
}
