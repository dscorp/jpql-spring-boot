package com.ds.restaurant.model.modeloandroid;


import java.io.Serializable;

public class Mesa implements Serializable {

	private static final long serialVersionUID = -7680732791926319401L;

	private Integer numero;
	
	private Integer tenedores;

	private Boolean estado;

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getTenedores() {
		return tenedores;
	}

	public void setTenedores(Integer tenedores) {
		this.tenedores = tenedores;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Mesa{" +
				"numero=" + numero +
				", tenedores=" + tenedores +
				", estado=" + estado +
				'}';
	}
}