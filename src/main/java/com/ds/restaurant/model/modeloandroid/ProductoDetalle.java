package com.ds.restaurant.model.modeloandroid;

import java.io.Serializable;
import java.util.List;

public class ProductoDetalle  implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	private String descripcion;
	private double precio;
	private boolean vigencia;

	private Producto producto;
	private List<PedidoDetalle> detalles;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public boolean isVigencia() {
		return vigencia;
	}


	public void setVigencia(boolean vigencia) {
		this.vigencia = vigencia;
	}


	public Producto getProducto() {
		return producto;
	}


	public void setProducto(Producto producto) {
		this.producto = producto;
	}


	public List<PedidoDetalle> getDetalles() {
		return detalles;
	}


	public void setDetalles(List<PedidoDetalle> detalles) {
		this.detalles = detalles;
	}
	
	
	


}
