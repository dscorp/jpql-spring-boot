package com.ds.restaurant.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Mesa implements Serializable {

	@Id
	private int numero;

	private int tenedores;

	private boolean estado;

	@JsonIgnore
	@OneToMany(mappedBy = "mesa",fetch = FetchType.LAZY)
	private List<Pedido> pedidos;
	
	
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getTenedores() {
		return tenedores;
	}

	public void setTenedores(int tenedores) {
		this.tenedores = tenedores;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
 

	

	
	
	

}
