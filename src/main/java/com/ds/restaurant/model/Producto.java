package com.ds.restaurant.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Producto implements Serializable {

	@GeneratedValue
	@Id
	private int id;
	private String nombre;
	private String descripcion;
	private String imagen;
	private int valoracion;




	@OneToOne()
	@JoinColumn(name = "idProductoCategoria")
	private ProductoCategoria categoria;
	
	@JsonIgnore
	@OneToMany(mappedBy = "producto", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private List<ProductoDetalle> detalles;


	@OneToOne ()
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getValoracion() {
		return valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}

	public ProductoCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(ProductoCategoria categoria) {
		this.categoria = categoria;
	}

	public List<ProductoDetalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<ProductoDetalle> detalles) {
		this.detalles = detalles;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario; 
	}


	 
	

	
}
