INSERT INTO `Usuario` VALUES (1, '1234', 'rikoschiken', 'rikos');
INSERT INTO `ProductoCategoria` VALUES (1, 'bebidas', 'beasd', 'bebidas');
INSERT INTO `ProductoCategoria` VALUES (2, 'comida', 'asda', 'comida');
INSERT INTO `Mesa` VALUES (1, b'0', 4);
INSERT INTO `Mesa` VALUES (2, b'0', 6);
INSERT INTO `Producto` VALUES (611, ' El mostrito es una combinación de arroz chaufa y 1/8 de pollo a la brasa. El mostrazo se diferencia del anterior, solo en que es más abundante', 'mostrito.jpg', 'Mostrito', 2, 2, 1);
INSERT INTO `Producto` VALUES (612, 'Pollo a la brasa es la denominación que se da en el Perú al pollo asado al carbón, a la lena o a gas en un sistema rotatorio', 'pollobrasa.jpg', 'Pollo a la  braza', 2, 2, 1);
INSERT INTO `Producto` VALUES (613, 'Inca Kola es una bebida gaseosa originaria del Perú. Aunque es consumida, principalmente en el Perú', 'incakola.jpg', 'Inka Kola', 4, 1, 1);
INSERT INTO `Producto` VALUES (614, 'Coca-Cola, conocida comúnmente como Coca en muchos países hispanohablantes, es una bebida gaseosa y refrescante, vendida a nivel mundial, en tiendas, restaurantes y máquinas expendedoras en más de doscientos países o territorios.', 'cocacola.jpg', 'Coca Cola', 4, 1, 1);
INSERT INTO `Producto` VALUES (615, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ulla', 'arrozconpollo.jpg', 'Arroz Con Pollo', 2, 2, 1);

INSERT INTO `ProductoDetalle` VALUES (11, 'asd', 23, b'1', 611);
INSERT INTO `ProductoDetalle` VALUES (12, 'sadasd', 33, b'1', 612);
INSERT INTO `ProductoDetalle` VALUES (13, 'asdasd', 32, b'1', 613);
INSERT INTO `ProductoDetalle` VALUES (14, 'asd', 2, b'1', 614);
INSERT INTO `ProductoDetalle` VALUES (115, 'asd', 2, b'1', 615);

INSERT INTO `restaurantandroid`.`empleado`(`id`, `cargo`, `contrasena`, `dni`, `nombre`, `usuariologin`, `idUsuario`) VALUES (2, 'ROLE_MESERO', '$2a$10$cbnMx/Bq8zFKiferm5viy.7k8vKL90IHuEmKhFWovH3lr/3THeLlS', '12312321', 'mesero', 'mesero', 1);
INSERT INTO `restaurantandroid`.`empleado`(`id`, `cargo`, `contrasena`, `dni`, `nombre`, `usuariologin`, `idUsuario`) VALUES (1, 'ROLE_COCINERO', '$2a$10$cbnMx/Bq8zFKiferm5viy.7k8vKL90IHuEmKhFWovH3lr/3THeLlS', '12312321', 'cocinero', 'cocinero', 1);